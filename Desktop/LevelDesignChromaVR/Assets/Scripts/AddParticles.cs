﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddParticles : MonoBehaviour {

    Color color;
    Transform[] cubes;
    ParticleSystem sparks;
    Transform particle;
    ParticleSystem.Particle[] particles;
    ParticleSystem.ColorOverLifetimeModule col;

    float pitch;

	// Use this for initialization
	void Start () {
        cubes = new Transform[transform.childCount];
        for (int i = 0; i < cubes.Length; ++i)
            cubes[i] = transform.GetChild(i);
	}
	
	// Update is called once per frame
	void Update ()
    {/*
        if(cubes.Length == 0)
        {
            cubes = new Transform[transform.childCount];
            for (int i = 0; i < cubes.Length; ++i)
                cubes[i] = transform.GetChild(i);
            pitch = GetComponent<DSP_Frequency_Analysis>().setPitch;
        }
        
        foreach (Transform cube in cubes)
        {
            // down particle
            particle = cube.GetChild(0);
            sparks = cube.GetChild(0).GetComponent<ParticleSystem>();
            var main = sparks.main;
            pitch = GetComponent<DSP_Frequency_Analysis>().setPitch;
            main.startColor = new Color(0.5f * pitch,-(pitch*pitch) + 2 * pitch, 2 - pitch, Random.Range(0f, 1f));
            particle.localScale = new Vector3(particle.localScale.x, particle.localScale.y, cube.localScale.y / 10);
            
            // up particle
            particle = cube.GetChild(1);
            sparks = cube.GetChild(1).GetComponent<ParticleSystem>();
            main = sparks.main;
            pitch = GetComponent<DSP_Frequency_Analysis>().setPitch;
            main.startColor = new Color(0.5f * pitch, -(pitch * pitch) + 2 * pitch, 2 - pitch, Random.Range(0.3f, 1f));
            particle.localScale = new Vector3(particle.localScale.x, particle.localScale.y, cube.localScale.y / 10);
        }*/
	}
}
