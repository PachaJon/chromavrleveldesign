﻿/* 
 name: David Keller
 course: CST306
 */
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using UnityEngine;
using System;

public class DSP_Frequency_Analysis : MonoBehaviour {

    FMOD.System low_level_system;
    FMOD.RESULT contaner;
    FMOD.DSP FFTDSP;
    FMOD.DSP pitchshift;
    FMOD.DSP tone;
    FMOD.Sound song;
    FMOD.Channel channel;
    FMOD.DSP_PARAMETER_FFT fft;
    FMOD.ChannelGroup master;
    FMOD.ChannelGroup sub;
    FMOD.DSPConnection test;

    uint length;
    public GameObject box;
    [Range(0.0f, 2.0f)]
    public float setPitch;
    [Range(1.0f,15000f)]
    public float fre;


    // Use this for initialization
    void Start () {
        Initialize();
        Vector3 p = transform.position;
        for (int i = 0; i < 1024; i++)
        {
            Instantiate(box, p, transform.rotation, transform);
            p.z = p.z + 0.1f;
        }
        setPitch = 1.0f;
        fre = 1.0f;
        
    }
	
	// Update is called once per frame
	void Update () {
		for(int i =0; i <1024; i++)
        {
            System.IntPtr tempptr;
            FFTDSP.getParameterData(2, out tempptr, out length);
            fft = (FMOD.DSP_PARAMETER_FFT)Marshal.PtrToStructure(tempptr, typeof(FMOD.DSP_PARAMETER_FFT));

            float temp = fft.spectrum[0][i];
            transform.GetChild(i).localScale = new Vector3(transform.GetChild(i).localScale.x, 400.0f * temp, transform.GetChild(i).localScale.z);

        }

        tone.setParameterFloat(1, fre);

        pitchshift.setParameterFloat((int)FMOD.DSP_PITCHSHIFT.PITCH, setPitch);
    }

    


    public void Initialize()
    {
        //getting the system handle
        low_level_system = FMODUnity.RuntimeManager.LowlevelSystem;
        uint version;
        low_level_system.getVersion(out version);
        //opening the sound stream
        FMOD.MODE m = FMOD.MODE.CREATESTREAM | FMOD.MODE.LOOP_NORMAL;
        FMOD.CREATESOUNDEXINFO n = new FMOD.CREATESOUNDEXINFO();
        contaner = low_level_system.createStream("/song.mp3", m, ref n, out song);  
        low_level_system.createDSPByType(FMOD.DSP_TYPE.OSCILLATOR, out tone);
        
        Debug.Log(contaner);
        //attach the sound stream to a channel
        contaner = low_level_system.playSound(song, null, false, out channel);
        
        Debug.Log(contaner);
        

        
        

        

        low_level_system.createDSPByType(FMOD.DSP_TYPE.PITCHSHIFT, out pitchshift);
        pitchshift.setParameterFloat(0, setPitch);
        pitchshift.setParameterFloat(1, 4096f);

        //pitchshift.addInput(tone, out test, FMOD.DSPCONNECTION_TYPE.MAX);
        //low_level_system.playDSP(tone, null, false, out channel);
        //channel.addDSP(0, pitchshift);

        low_level_system.createDSPByType(FMOD.DSP_TYPE.FFT, out FFTDSP);
        
        //FFTDSP.addInput(pitchshift, out test, FMOD.DSPCONNECTION_TYPE.MAX);
        
        

        //initialize the channelgroup sub
        low_level_system.createChannelGroup("sub", out sub);

        //sub.addDSP(0, pitchshift);
        //grab the tail of the channelgroup sub
        FMOD.DSP tail;
        //sub.getDSP(FMOD.CHANNELCONTROL_DSP_INDEX.TAIL, out tail);
        //tail.addInput(FFTDSP, out test, FMOD.DSPCONNECTION_TYPE.STANDARD);
        //sub.getDSP(FMOD.CHANNELCONTROL_DSP_INDEX.TAIL, out tail);
        sub.addDSP(FMOD.CHANNELCONTROL_DSP_INDEX.TAIL, pitchshift);
        sub.addDSP(FMOD.CHANNELCONTROL_DSP_INDEX.HEAD, FFTDSP);
        

        // grab the head of the channel channel
        FMOD.DSP head;
        channel.getDSP(FMOD.CHANNELCONTROL_DSP_INDEX.HEAD, out head);
        pitchshift.addInput(head, out test, FMOD.DSPCONNECTION_TYPE.STANDARD);



        System.IntPtr temp;
        FFTDSP.getParameterData(2, out temp, out length);
        fft = (FMOD.DSP_PARAMETER_FFT) Marshal.PtrToStructure(temp, typeof(FMOD.DSP_PARAMETER_FFT));
        FFTDSP.setActive(true);
        pitchshift.setActive(true);

        
    }
}
