﻿/*

name: Tom Galbert

course: CST306

*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddParticles : MonoBehaviour {

    public int randColorMin = 0;
    public int randColorMax = 100;
    public bool randBool = false;

    Color color;
    Transform[] cubes;
    ParticleSystem sparks;
    Transform particle;
    ParticleSystem.Particle[] particles;
    ParticleSystem.ColorOverLifetimeModule col;

    float pitch;

	// Use this for initialization
	void Start () {
        cubes = getSoundShape();
        pitch = GetComponent<DSP_Frequency_Analysis>().setPitch;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(cubes.Length == 0)
        {
            cubes = new Transform[transform.childCount];
            for (int i = 0; i < cubes.Length; ++i)
            {
                cubes[i] = transform.GetChild(i);
            }
            pitch = GetComponent<DSP_Frequency_Analysis>().setPitch;
        }
        
        foreach (Transform cube in cubes)
        {
            /* down particle */
            particle = cube.GetChild(0);
            sparks = cube.GetChild(0).GetComponent<ParticleSystem>();
            var main = sparks.main;
            pitch = GetComponent<DSP_Frequency_Analysis>().setPitch;
            if(randBool)
            {
                main.startColor = new Color(0.5f * pitch* initRedRandom(), -(pitch * pitch) + 2 * pitch, 2 - pitch * initBlueRandom(), Random.Range(0.1f, 1f));
            }
            else
            {
                main.startColor = new Color(0.5f * pitch, -(pitch * pitch) + 2 * pitch, 2 - pitch, Random.Range(0.1f, 1f));
            }
            particle.localScale = new Vector3(getParticleSize(cube.localScale.z), getParticleSize(cube.localScale.x), cube.localScale.y / 11);
            //particle.localScale = new Vector3(particle.localScale.x, particle.localScale.y, cube.localScale.y / 11);
            
            /* up particle */
            particle = cube.GetChild(1);
            sparks = cube.GetChild(1).GetComponent<ParticleSystem>();
            main = sparks.main;
            pitch = GetComponent<DSP_Frequency_Analysis>().setPitch;
            if (randBool)
            {
                main.startColor = new Color(0.5f * pitch * initRedRandom(), -(pitch * pitch) + 2 * pitch, 2 - pitch * initBlueRandom(), Random.Range(0.1f, 1f));
            }
            else
            {
                main.startColor = new Color(0.5f * pitch, -(pitch * pitch) + 2 * pitch, 2 - pitch, Random.Range(0.1f, 1f));
            }
            particle.localScale = new Vector3(getParticleSize(cube.localScale.z), getParticleSize(cube.localScale.x), cube.localScale.y / 11);
            //particle.localScale = new Vector3(particle.localScale.x, particle.localScale.y, cube.localScale.y / 11);
        }
	}

    private Transform[] getSoundShape()
    {
        Transform[] res = new Transform[transform.childCount];
        for (int i = 0; i < cubes.Length; ++i)
        {
            cubes[i] = transform.GetChild(i);
        }
        return res;
    }

    private float getParticleSize(float cubeSize)
    {
        //temporary
        return cubeSize / 20;
    }

    float initRedRandom()
    {
        float min = randColorMin / 100;
        float max = randColorMax / 100;
        float ran = Random.Range(min, max);
        return ran;
    }

    float initBlueRandom()
    {
        float min = randColorMin / 100;
        float max = randColorMax / 100;
        float ran = Random.Range(min, max);
        return ran;
    }
}
