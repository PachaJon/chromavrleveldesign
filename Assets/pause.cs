﻿/*

name: Tom Galbert

course: CST306

*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pause : MonoBehaviour {

    bool paused;
    GUIText Image;

    // Use this for initialization
    void Start() {
        paused = false;
        Image = GetComponent<GUIText>();
    }

    // Update is called once per frame
    void Update() {
        if(Input.GetKeyDown(KeyCode.P))
        {
            toPause();
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            unPause();
        }
    }

    void toPause()
    {
        Time.timeScale = 0;
        Image.text = "Pause";
    }

    void unPause()
    {
        Time.timeScale = 1;
        Image.text = "";
    }
}
