﻿/* 
 name: David Keller
 course: CST306
 */
using System;
using System.Collections;
using System.Collections.Generic;
using FMOD;
using UnityEngine;
using System.Runtime.InteropServices;

public class FFT : DSP {
    FMOD.DSP_PARAMETER_FFT fft;
    private uint length;
    FMOD.RESULT result;
    public GameObject box;


    public override void addInput(FMOD.DSP input)
    {
        result = dsp.addInput(input, out connection, DSPCONNECTION_TYPE.STANDARD);
    }

    public void initalize()
    {
        init();

        low_level_system.createDSPByType(DSP_TYPE.FFT, out dsp);
        System.IntPtr temp;
        result = dsp.setParameterInt((int)FMOD.DSP_FFT.WINDOWSIZE, 1024);
        result = dsp.getParameterData(2, out temp, out length);
        fft = (FMOD.DSP_PARAMETER_FFT)Marshal.PtrToStructure(temp, typeof(FMOD.DSP_PARAMETER_FFT));
        dsp.setBypass(true);

        Vector3 p = transform.position;
        for (int i = 0; i < length; i++)
        {
            Instantiate(box, p, transform.rotation, transform);
            p.z = p.z + 0.1f;
        }
    }
    public void SetActive()
    {
        dsp.setBypass(false);
    }
    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        bool active;
        dsp.getBypass(out active);
        //UnityEngine.Debug.Log(active);
        if (!active)
        {
            System.IntPtr tempptr;
            dsp.getParameterData((int)FMOD.DSP_FFT.SPECTRUMDATA, out tempptr, out length);
            fft = (FMOD.DSP_PARAMETER_FFT)Marshal.PtrToStructure(tempptr, typeof(FMOD.DSP_PARAMETER_FFT));
            for (int i = 0; i < length; i++)
            {
                float temp = fft.spectrum[0][i];
                transform.GetChild(i).localScale = new Vector3(transform.GetChild(i).localScale.x, 400.0f * temp, transform.GetChild(i).localScale.z);
            }
        }
    }
}
