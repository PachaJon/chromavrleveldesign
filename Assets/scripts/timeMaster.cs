﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Diagnostics;


public class timeMaster : MonoBehaviour {

    int maxNumSounds = 20;
    public static uint maxtime =120*1000;

    public Slider mastertime;
    Soundinfo[] sounds;
    Queue<int> openSounds;
    List<int> activeSounds;

    uint currentPositionInTimeLine;
    uint timeOffset;
    Stopwatch accurateTime;
    bool isPlaying = false;

    //this is for testing
    public GameObject soundcontrollerobject;
    public channelgroup channelgroup;
    public GameObject channelgroupObject;

    public RectTransform masterTimeSliderWidth;



	// Use this for initialization
	void Start () {
        accurateTime = new Stopwatch();
        UnityEngine.Debug.Log("Is High Resolution: " + Stopwatch.IsHighResolution);
        UnityEngine.Debug.Log("Stopwatch Frequency: " + Stopwatch.Frequency);
        //accurateTime.Start();
        sounds = new Soundinfo [maxNumSounds];
        openSounds = new Queue<int>();
        activeSounds = new List<int>();

        masterTimeSliderWidth = gameObject.GetComponent<RectTransform>();
        mastertime = GameObject.FindGameObjectWithTag("Master Time value").GetComponent<Slider>();

        for(int i =0; i < maxNumSounds; i++)
        {
            openSounds.Enqueue(i);
        }
        currentPositionInTimeLine = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (isPlaying)
        {
            currentPositionInTimeLine = timeOffset + (uint)accurateTime.ElapsedMilliseconds;
            foreach(int sound in activeSounds)
            {
               
                if (sounds[sound].start < currentPositionInTimeLine && sounds[sound].end > currentPositionInTimeLine)
                {
                    if (sounds[sound].sound.isPaused())
                    {
                        sounds[sound].sound.playFrom(currentPositionInTimeLine - sounds[sound].start);
                        sounds[sound].isPlaying = true;
                    }
                }
                else
                {
                    sounds[sound].isPlaying = false;
                    sounds[sound].sound.pause();
                }
            }
        }
        mastertime.value = (float)currentPositionInTimeLine / (float)maxtime;
    }

    public void addToTimeLine(SoundControler s, Slider test)
    {
        Soundinfo temp = new Soundinfo();
        temp.soundPos = openSounds.Dequeue();
        activeSounds.Add(temp.soundPos);
        temp.sound = s;
        SoundControler.DEL update = temp.update;
        temp.sound.setUpdate(update);
        temp.checkforChange = checkForChange;
        temp.timePosition = test;
        temp.timePosition.onValueChanged.AddListener(delegate
        {
            temp.update();
        });

        
        sounds[temp.soundPos] = temp;
        temp.update();
    }

    void checkForChange(int index)
    {
        if (sounds[index].start < currentPositionInTimeLine && sounds[index].end > currentPositionInTimeLine)
        {
            sounds[index].sound.playFrom(currentPositionInTimeLine - sounds[index].start);
            sounds[index].isPlaying = true;
        }
        else
        {
            sounds[index].sound.pause();
        }


        sounds[index].timePosition.transform.GetChild(2).GetChild(0).GetComponent<RectTransform>().offsetMax = new Vector2( masterTimeSliderWidth.rect.width,10);
        UnityEngine.Debug.Log(sounds[index].timePosition.transform.GetChild(2).GetChild(0).GetComponent<RectTransform>().sizeDelta);
    }

    

    public void play()
    {
        isPlaying = true;
        accurateTime.Start();
    }
    public void pause()
    {
        isPlaying = false;
        timeOffset = currentPositionInTimeLine;
        accurateTime.Stop();
        accurateTime.Reset();

        foreach(int sound in activeSounds)
        {
            sounds[sound].sound.pause();
        }
    }


}

//++++++++++++++++++++++++++++++++++ SUPPORT STRUCT ++++++++++++++++++++++++++++++++++++++//
public class Soundinfo
{
    public SoundControler sound;
    public uint length;
    public uint start;
    public uint end;
    public Slider timePosition;
    public bool isPlaying;

    public int soundPos;
    public delegate void DEL(int index);
    public DEL checkforChange;
    //the channlegroup its attached to?
    public void update()
    {
        length = sound.getEnd() - sound.getStart();
        start = (uint)(timePosition.value * timeMaster.maxtime);
        end = start + length;

            checkforChange(soundPos);
    }
}
