﻿/* 
 name: David Keller
 course: CST306
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMOD;

public class channelgroup : MonoBehaviour {
    public FMOD.ChannelGroup group;
    protected FMOD.System low_level_system;
    protected uint version;
    protected FMOD.DSPConnection connection;
    LineRenderer lineRenderer;
    List<DSP> dspArray;
    public GameObject fftPrefab;
    public GameObject pitchshiftPrefab;
    public GameObject SoundPrefab;
    FMOD.RESULT result;

    //global timeline test
    uint currentpos;

    //temp
    FMOD.Sound song;
    FMOD.Channel chan;
    VECTOR pos;
    VECTOR posl;
    VECTOR vel;
    VECTOR other;
    VECTOR up;
    VECTOR forward;
    public float x=0;
    public float y=0;
    public float z=0;

    public void init()
    {
        //get the low level system modual
        low_level_system = FMODUnity.RuntimeManager.LowlevelSystem;
        low_level_system.getVersion(out version);

        low_level_system.createChannelGroup(name, out group);
        low_level_system.set3DSettings(1, 1, 0.5f);

        FMOD.ChannelGroup master;
        low_level_system.getMasterChannelGroup(out master);
        master.addGroup(group, false, out connection);

        up.x = 0;
        up.y = 1;
        up.z = 0;
        forward.x = 0;
        forward.y = 0;
        forward.z = 1;
        pos.x = 0;
        pos.y = 0;
        pos.z = 0;
        posl.x = x;
        posl.y = y;
        posl.z = z;
        vel.x = 0;
        vel.y = 0;
        vel.z = 0;
        group.set3DAttributes(ref pos, ref vel, ref other);
        low_level_system.set3DListenerAttributes(0, ref posl, ref vel, ref forward, ref up);


        lineRenderer = gameObject.GetComponent<LineRenderer>();
        dspArray = new List<DSP>();
    }

    // Use this for initialization
    void Start () {


     //this is temp stuff for rignt now
        GameObject pitchshift = Instantiate(pitchshiftPrefab, transform);
        pitchshift.GetComponent<pitchshift>().initalize();
        addDSP(pitchshift.GetComponent<pitchshift>());
        pitchshift.transform.SetAsFirstSibling();

        GameObject fft = Instantiate(fftPrefab, transform);
        fft.GetComponent<FFT>().initalize();
        addDSP(fft.GetComponent<FFT>());
        fft.transform.SetAsFirstSibling();

        //all 3d stuffs
        //group.set3DAttributes(ref pos, ref vel, ref other);
        //low_level_system.set3DListenerAttributes(0, ref posl, ref vel, ref forward, ref up);
    }
    // Update is called once per frame
    void Update () {
        posl.x = x;
        posl.y = y;
        posl.z = z;
        //low_level_system.set3DListenerAttributes(0, ref posl, ref vel, ref forward, ref up);

        low_level_system.update();

        int test;
        group.getNumChannels(out test);
        if(test > 0)
        {
            FindObjectOfType<FFT>().SetActive();
        }


    }
    void addDSP(DSP dsp)
    {
        group.addDSP(FMOD.CHANNELCONTROL_DSP_INDEX.TAIL, dsp.dsp);
        group.getDSPIndex(dsp.dsp, out dsp.index);
        dspArray.Add(dsp);
    }

    public void createSound(string fileName)
    {
        GameObject sound = Instantiate(SoundPrefab, transform);
        sound.GetComponent<Sound>().init();
        sound.GetComponent<Sound>().loadSoundOnGroup(fileName, this);
        sound.GetComponent<Sound>().play();
    }

    void addDSPToIndex(DSP dsp, int index)
    {
        FMOD.RESULT check = group.addDSP(FMOD.CHANNELCONTROL_DSP_INDEX.FADER + index, dsp.dsp);
        if (check != FMOD.RESULT.OK)
        {
            group.addDSP(FMOD.CHANNELCONTROL_DSP_INDEX.TAIL, dsp.dsp);
        }
        group.getDSPIndex(dsp.dsp, out dsp.index);
        dspArray.Insert(dsp.index - 1, dsp); // this line may not be quite right
    }

    private void OnDestroy()
    {
        group.release();
    }
}
