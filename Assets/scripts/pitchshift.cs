﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pitchshift : DSP {

    FMOD.RESULT result;
    public float pitch;


    public override void addInput(FMOD.DSP input)
    {
        result = dsp.addInput(input, out connection, FMOD.DSPCONNECTION_TYPE.STANDARD);
    }

    public void initalize()
    {
        init();
        low_level_system.createDSPByType(FMOD.DSP_TYPE.PITCHSHIFT, out dsp);

        dsp.setParameterFloat((int)FMOD.DSP_PITCHSHIFT.FFTSIZE, 1024f);
        setPitch(1);
    }
    public void setPitch(float newpitch)
    {
        dsp.setParameterFloat((int)FMOD.DSP_PITCHSHIFT.PITCH, newpitch);
        pitch = newpitch;
    }



    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        dsp.setParameterFloat((int)FMOD.DSP_PITCHSHIFT.PITCH, pitch);
    }
}
