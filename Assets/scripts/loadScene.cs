﻿/*
 * Garrett Tibbetts
 * CST 306
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class loadScene : MonoBehaviour {

    public Button[] sceneButtons;
    public Button piano, editor, menu;
    public GameObject[] objects;
    public string sceneName, currentName;
    // Use this for initialization
    void Start ()
    {
        SceneManager.LoadSceneAsync("menu");
        SceneManager.SetActiveScene(SceneManager.GetSceneByName("menu"));

        sceneButtons = GameObject.FindObjectsOfType<Button>();
        objects = GameObject.FindObjectsOfType<GameObject>();

		foreach(Button b in sceneButtons)
        {
            if(this.tag == "piano" && b.tag == "piano")
            {
                piano = b;
                sceneName = "playScene";
                currentName = "menu";
            }

            else if(b.tag == "editor" && this.tag == "editor")
            {
                editor = b;
                sceneName = "TimeLine";
                currentName = "menu";
            }

            else if (b.tag == "menu" && this.tag == "menu")
            {
                menu = b;
                sceneName = "menu";
                if(SceneManager.GetActiveScene().name == "playScene")
                {
                    currentName = "playScene";
                }

                else
                {
                        currentName = "TimeLine";
                }
                
            }
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void onClick()
    {
        SceneManager.UnloadSceneAsync(currentName);
        SceneManager.LoadSceneAsync(sceneName);
        SceneManager.SetActiveScene(SceneManager.GetSceneByName(sceneName));
    }
}
