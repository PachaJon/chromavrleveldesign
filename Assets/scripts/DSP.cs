﻿/* 
 name: David Keller
 course: CST306
 */
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using System;
using FMOD;

public abstract class DSP : MonoBehaviour
{
    public FMOD.DSP dsp;
    protected FMOD.System low_level_system;
    protected uint version;
    protected FMOD.DSPConnection connection;
    public int index;

    public void init()
    {
        low_level_system = FMODUnity.RuntimeManager.LowlevelSystem;
        low_level_system.getVersion(out version);   
    }

    abstract public void addInput(FMOD.DSP input);

    public void setParamaterFloat(int para, float value)
    {
        dsp.setParameterFloat(para, value);
    }

    public void OnDestroy()
    {

    }
}