﻿/* 
 name: David Keller
 course: CST306
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class FileLoadController : MonoBehaviour {

    public GameObject button;
    List<string> songFiles;
    List<DirectoryInfo> directorylist;
    GameObject[] buttons;
    float offset = 5;

    //used for instantiating new sounds
    timeMaster timemaster;
    public GameObject soundControllerPrefab;
    public GameObject slider;
    public channelgroup loadon; //this is a temp

	// Use this for initialization
	void Start () {
        buttons = new GameObject[100];
        searchDirectory("/");
        timemaster = GameObject.FindGameObjectWithTag("Time Box").GetComponent<timeMaster>();
    }


    // Update is called once per frame
    void Update () {
		
	}

    void searchDirectory(string s)
    {
        songFiles = new List<string>();
        directorylist = new List<DirectoryInfo>();
        foreach(GameObject b in buttons)
        {
            Destroy(b);
        }

        DirectoryInfo d = new DirectoryInfo(s);
        FileInfo[] files = d.GetFiles();
        DirectoryInfo[] directoies = d.GetDirectories();

            Quaternion temp0 = transform.rotation;
            temp0.eulerAngles = new Vector3(0, 90, 0);
            buttons[0] = Instantiate(button, transform);
            buttons[0].GetComponent<RectTransform>().rotation = temp0;

            buttons[0].GetComponent<Button>().onClick.AddListener(delegate
            {
                searchDirectory(d.Parent.FullName);
            });
            buttons[0].transform.GetChild(0).GetChild(0).GetComponent<Text>().text = "./";
            //buttons[0].GetComponent<RectTransform>().anchoredPosition = new Vector2(0, (-1 * 10 * 0) - offset);
            //buttons[0].GetComponent<RectTransform>().sizeDelta = new Vector2(-2, 11);
            Vector3 newpos0 = buttons[0].GetComponent<RectTransform>().localPosition;
            newpos0.z = 0;
            buttons[0].GetComponent<RectTransform>().localPosition = newpos0;


        int index = 1;
        foreach (DirectoryInfo dir in directoies)
        {
            Quaternion temp = transform.rotation;
            temp.eulerAngles = new Vector3(0, 90, 0);
            buttons[index] = Instantiate(button, transform);
            buttons[index].GetComponent<RectTransform>().rotation = temp;
            int captureditorator = index;
            buttons[index].GetComponent<Button>().onClick.AddListener(delegate
            {
                searchDirectory(dir.FullName);
                Debug.Log("loading directory");
            });
            buttons[index].transform.GetChild(0).GetChild(0).GetComponent<Text>().text = dir.Name;
            //buttons[index].GetComponent<RectTransform>().anchoredPosition = new Vector2(0, (-1 * 10 * index) - offset);
            //buttons[index].GetComponent<RectTransform>().sizeDelta = new Vector2(-2, 11);
            Vector3 newpos = buttons[index].GetComponent<RectTransform>().localPosition;
            newpos.z = 0;
            buttons[index].GetComponent<RectTransform>().localPosition = newpos;
            index++;
        }
        //offset = directoies.Length * 10;
        
        foreach (FileInfo f in files)
        {
            string[] descriptor = f.Name.Split(".".ToCharArray());
            if (descriptor[descriptor.Length - 1] == "mp3")
            {
                //Debug.Log(f.FullName);
                songFiles.Add(f.FullName);
            }
        }

        int songfileIndex = 0;
        foreach (string song in songFiles)
        {

            Quaternion temp = transform.rotation;
            temp.eulerAngles = new Vector3(0, 90, 0);
            buttons[index] = Instantiate(button, transform);
            buttons[index].GetComponent<RectTransform>().rotation = temp;

            int captureditorator = songfileIndex;
            buttons[index].GetComponent<Button>().onClick.AddListener(delegate
            {
                //playsong(captureditorato
                loadfile(captureditorator);

            });
            buttons[index].transform.GetChild(0).GetChild(0).GetComponent<Text>().text = song; // TODO: make it so that the whole file system isnt displayed
            //buttons[index].GetComponent<RectTransform>().anchoredPosition = new Vector2(0, (-1 * 10 * index) - offset);
            //buttons[index].GetComponent<RectTransform>().sizeDelta = new Vector2(-2, 11);
            Vector3 newpos = buttons[index].GetComponent<RectTransform>().localPosition;
            newpos.z = 0;
            buttons[index].GetComponent<RectTransform>().localPosition = newpos;
            //buttons[index].GetComponent<RectTransform>().
            //buttons[index].GetComponent<RectTransform>().offsetMax;
            index++;
            songfileIndex++;
        }
        transform.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(100, 60 * index);
        Vector3 panlepos =  GetComponent<RectTransform>().position;
        panlepos.y = -index * 5;
        //GetComponent<RectTransform>().position = panlepos;
    }


    void loadfile(int index)
    {
        GameObject soundControllerObject =Instantiate(soundControllerPrefab);
        soundControllerObject.transform.parent = GameObject.FindGameObjectWithTag("Loaded Sounds").transform;
        SoundControler temp = soundControllerObject.GetComponent<SoundControler>();
        temp.group = loadon;
        temp.init();

        GameObject tempSlider = Instantiate(slider, GameObject.FindGameObjectWithTag("Master Time slider").transform);
        tempSlider.transform.rotation = GameObject.FindGameObjectWithTag("Master Time slider").transform.rotation;

        timemaster.addToTimeLine(temp, tempSlider.GetComponent<Slider>());
    }
    void playsong(int index)
    {
        FindObjectOfType<channelgroup>().GetComponent<channelgroup>().createSound(songFiles[0]);
    }
}
