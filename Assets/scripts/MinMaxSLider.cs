﻿/* 
 name: David Keller
 course: CST306
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class MinMaxSLider : MonoBehaviour {
    public float min;
    public float max;
    Slider minSlider;
    Slider maxSlider;

    public delegate void DEL();
    private bool updateisSet = false;
    DEL update;

    // Use this for initialization
    void Start () {
        minSlider = transform.GetChild(0).GetComponent<Slider>();
        maxSlider =  transform.GetChild(1).GetComponent<Slider>();
        //transform.parent.GetComponent<>

        minSlider.onValueChanged.AddListener(delegate
        {
            minUpdate();
        });
        maxSlider.onValueChanged.AddListener(delegate
        {
            maxUpdate();
        });
        min = 0;
        max = 1;

        minSlider.value = min;
        maxSlider.value = max-1;
    }
    public void maxUpdate()
    {
        if(1-maxSlider.value < minSlider.value)
        {
            maxSlider.value = 1 - minSlider.value;
        }
        max = 1-maxSlider.value;
        if (updateisSet)
        {
            update();
        }
        
    }
    public void minUpdate()
    {
        if(minSlider.value > 1-maxSlider.value)
        {
            minSlider.value = 1 - maxSlider.value;
        }
        min = minSlider.value;
        if (updateisSet)
        {
            update();
        }
    }
    // Update is called once per frame
    void Update () {
        
	}

    public void setUpdate(DEL d)
    {
        update = d;
        Debug.Log("setUpdate");
        updateisSet = true;
    }
   
}
