﻿/* 
 name: David Keller
 course: CST306
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMOD;

public class Sound : MonoBehaviour {
    FMOD.Sound sound;
    FMOD.RESULT result;
    protected FMOD.System low_level_system;
    protected uint version;
    public string path;
    FMOD.Channel channel;


    

    //3d sound stuff
    public FMOD.VECTOR pos;
    public FMOD.VECTOR vel;

    //debugging for loop possitions
    public uint start;
    public uint end;
    public uint length;
    public uint currentPos;
    bool loop;
    int loopCount;

    public void init()
    {
        low_level_system = FMODUnity.RuntimeManager.LowlevelSystem;
        low_level_system.getVersion(out version);
    }
    public void loadSound(string path)
    {
        this.path = path;
        FMOD.CREATESOUNDEXINFO n = new FMOD.CREATESOUNDEXINFO();
        FMOD.MODE m = FMOD.MODE.CREATESTREAM | FMOD.MODE.LOOP_NORMAL | FMOD.MODE._2D;// | FMOD.MODE._3D_LINEARROLLOFF;
        result = low_level_system.createStream(path, m, ref n, out sound);
        result = low_level_system.playSound(sound, null, false, out channel);
    }
    public void loadSound()
    {
        FMOD.CREATESOUNDEXINFO n = new FMOD.CREATESOUNDEXINFO();
        FMOD.MODE m = FMOD.MODE.CREATESTREAM | FMOD.MODE.LOOP_NORMAL | FMOD.MODE._3D;
        result = low_level_system.createStream(path, m, ref n, out sound);
        result = low_level_system.playSound(sound, null, false, out channel);
    }
    public void loadSoundOnGroup(string path, channelgroup group)
    {
        this.path = path;
        FMOD.CREATESOUNDEXINFO n = new FMOD.CREATESOUNDEXINFO();
        FMOD.MODE m = FMOD.MODE.CREATESTREAM | FMOD.MODE.LOOP_NORMAL | FMOD.MODE._3D | FMOD.MODE._3D_LINEARROLLOFF;
        result = low_level_system.createStream(path, m, ref n, out sound);

        //sound.set3DMinMaxDistance(1f, 5000); //3d sound stuff
        pos.x = 0;
        pos.y = 0;
        pos.z = 1.5f;
        vel.x = 0;
        vel.y = 0;
        vel.z = 0;

        result = low_level_system.playSound(sound, group.GetComponent<channelgroup>().group, true, out channel);
        channel.set3DAttributes(ref pos, ref vel, ref vel);
        channel.setVolume(0.1f);
        UnityEngine.Debug.Log(result);

        sound.getLength(out length, TIMEUNIT.MS);
        start = 0;
        end = length;
    }

    public void pause()
    {
        channel.setPaused(true);
    }
    public void play()
    {
        channel.setPaused(false);
    }
    public void toggle()
    {
        bool ispaused;
        channel.getPaused(out ispaused);
        channel.setPaused(!ispaused);
    }

    public void changeGroup()
    {

    }

    public void setLoopPoints(float start, float end)
    {
        //sound.setLoopPoints((uint)(length * start), TIMEUNIT.MS, (uint)(length * end), TIMEUNIT.MS);
        this.start = (uint)(length * start);
        this.end = (uint)(length * end);
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        low_level_system.update();
        channel.getPosition(out currentPos, TIMEUNIT.MS);

        if (currentPos < start)
        {
            channel.setPosition(start, TIMEUNIT.MS);

        }
   
        //if (loop)  // will enable this later
        //{
        //    if (currentPos > end)
        //    {
        //        channel.setPosition(start, TIMEUNIT.MS);
        //    }
        //}
	}

    public void playFrom(uint time)
    {
        channel.setPosition(time, TIMEUNIT.MS);
        channel.setPaused(false);
    }
    public bool isPaused()
    {
        bool pause;
        channel.getPaused(out pause);
        return pause;
    }

    //public void setCurrentpos(uint)
    //{

    //}
    public void OnDestroy()
    {
        sound.release();
    }
}
