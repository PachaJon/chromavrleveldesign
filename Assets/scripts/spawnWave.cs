﻿/*
 * Garrett Tibbetts
 * CST 306
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class spawnWave : MonoBehaviour {

    public GameObject channel;

    public GameObject channelGroup;

	// Use this for initialization
	void Start ()
    {
        channel = Instantiate(channelGroup, GameObject.Find("Waveform").transform);

        channel.GetComponent<channelgroup>().init();
        channel.GetComponent<channelgroup>().createSound("C:/Users/VR Lab/Documents/chromavrleveldesign/Assets/It's Always Too Late to Start Over.mp3");
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (SceneManager.GetActiveScene() != SceneManager.GetSceneByName("menu"))
        {
            channel.GetComponent<channelgroup>().createSound("");
        }
	}
}
