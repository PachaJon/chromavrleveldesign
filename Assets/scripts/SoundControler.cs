﻿/* 
 name: David Keller
 course: CST306
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SoundControler : MonoBehaviour {
    Sound sound;
    Sound[] soundArray;
    MinMaxSLider minmax;  //this may be made to be pulic so it can be set by code

    public string filepath;
    public channelgroup group;

    public delegate void DEL();
    DEL update;
    GameObject soundObject;
    private bool updateisSet = false;

    //loop enabled (and count)

    // Use this for initialization
    public void Start () {

    }
    public void init()
    {
        //create a sound object
        soundObject = new GameObject();
        soundObject.transform.SetParent(transform);
        sound = soundObject.AddComponent<Sound>();
        sound.init();
        sound.loadSoundOnGroup(filepath, group);


        minmax = transform.GetChild(0).GetComponent<MinMaxSLider>();
        MinMaxSLider.DEL g = hasUpdated;
        minmax.setUpdate(g);
    }

    
	
	// Update is called once per frame
	void Update () {

	}
    public uint getStart()
    {
        return sound.start;
    }
    public uint getEnd()
    {
        return sound.end;
    }
    public void hasUpdated()
    {
        sound.setLoopPoints(minmax.min, minmax.max);
        if (updateisSet)
        {
            update();
        }
        //Debug.Log("hasUpdated");
    }

    public void playFrom(uint time)
    {
        sound.playFrom(time);
    }

    public bool isPaused()
    {
        return sound.isPaused();
    }

    public void pause()
    {
        sound.pause();
    }

    public void setUpdate(DEL inputFunction)
    {
        update = inputFunction;
        updateisSet = true;
    }
}
