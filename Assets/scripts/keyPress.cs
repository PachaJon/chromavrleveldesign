﻿/*
 * Garrett Tibbetts
 * CST 306
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Leap.Unity;

public class keyPress : MonoBehaviour
{
    Toggle choirToggle;
    public AudioSource choir, grand;
    bool fingerIN;

    // Use this for initialization
    void Start()
    {
        choirToggle = GameObject.FindObjectOfType<Toggle>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private bool handCollided(Collision other)
    {
        if( other.gameObject.tag == "finger tip")
        {
           
            return true;
        }

        else
        {
            return false;
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (fingerIN == false)
        {
            fingerIN = true;
            if (choirToggle.isOn && handCollided(col))
            {
                Debug.Log("enter");
                choir.Play();
            }

            else if (!choirToggle.isOn && handCollided(col))
            {
                grand.Play();
                Debug.Log("enter grand");
            }
        }
                
    }

    

    void OnCollisionExit(Collision col)
    {
        if (handCollided(col))
        {
            choir.Stop();
            grand.Stop();
            fingerIN = false;
            Debug.Log("exit");
        }
    }
}
